﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;


namespace SonarGraph
{
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct READ_SONARS
    {
        public UInt16 sonar1;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct WRITE_RX
    {
        public UInt16 CH4;
        public byte[] ToByteArray(){
            byte[] ar = new byte[7];
            byte checksum = 0;
            ar[0] = Form1.HEADER_BYTE_1;
            ar[1] = Form1.HEADER_BYTE_2;
            ar[2] = Form1.CMD_RX;
            checksum = (byte)(checksum + ar[2]);
            ar[3] = 2; //size
            checksum = (byte)(checksum + ar[3]);

            //spektrum sattelite sht
            ushort chan = (5 << 11);
            chan += CH4;

            //ar[4] = chan & 0x0000FFFF;

            return ar;
        }
    }


    public enum C_STATE
    {
        IDLE,
        HEADER1,
        HEADER2,
        HEADER_SIZE,
        HEADER_CMD
    };

    public partial class Form1 : Form
    {

        //*******Packet Commands*******/
        public const byte CMD_KEEP_ALIVE = 0xFF;
        public const byte CMD_SONARS = 0xA1;
        public const byte CMD_RX = 0xB1;

        //***end of Packet Commands****/

        const int MAX_POINTS = 500;
        int nextX = MAX_POINTS + 1;
        int maxX = 0;
        int minX = 0;
        int maxY = 5000;
        string RxChars = "";
        int cycles = 0;

        //Packet receiving routine
            public const byte HEADER_BYTE_1 = 0xFF;
            public const byte HEADER_BYTE_2 = 0xFE;
            public C_STATE c_state = C_STATE.IDLE;
            public byte dataSize, offset, checksum, cmd;
            public const int INBUF_SIZE = 64;
            public byte[] inBuf = new byte[INBUF_SIZE];
        //

        //Reading
            READ_SONARS sonars;
        //

        public Form1()
        {
            InitializeComponent();
            for (int i=0; i<MAX_POINTS; i++)
            {
                chart1.Series[0].Points.AddXY(i, 0);
            }

        }

        private void chart1_Click(object sender, EventArgs e)
        {
            if (!serialPort1.IsOpen)
            {
                serialPort1.PortName = "COM5";
                serialPort1.BaudRate = 57600;
                serialPort1.Open();
                if (serialPort1.IsOpen)
                {
                    chart1.BackColor = Color.White;
                }
            } else
            {
                serialPort1.Close();
                if (!serialPort1.IsOpen)
                {
                    chart1.BackColor = Color.Gray;
                }
            }
        }

        private void serialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            while (serialPort1.BytesToRead > 0)
            {
                //char c = (char)
                //RxChars = serialPort1.ReadLine();
                //this.Invoke(new EventHandler(processString));
                byte c = (byte)serialPort1.ReadByte();
                if (c_state == C_STATE.IDLE) {
                  c_state = (c==HEADER_BYTE_1) ? C_STATE.HEADER1 : C_STATE.IDLE;
                } 
                else if (c_state == C_STATE.HEADER1) {
                  c_state = (c==HEADER_BYTE_2) ? C_STATE.HEADER2 : C_STATE.IDLE;
                } 
                else if (c_state == C_STATE.HEADER2) {
                    cmd = c;
                    checksum = 0;
                    checksum = (byte)(checksum + c - 1);
                    c_state = C_STATE.HEADER_CMD;
                }
                else if (c_state == C_STATE.HEADER_CMD) {
                  if (c > INBUF_SIZE) {  // now we are expecting the payload size
                    c_state = C_STATE.IDLE;
                    return;
                  }
                  dataSize = c;
                  offset = 0;
                  checksum = (byte)(checksum + c - 1);
                  c_state = C_STATE.HEADER_SIZE;  // the command is to follow
                } 
                else if (c_state == C_STATE.HEADER_SIZE && offset < dataSize) {
                    checksum = (byte)(checksum + c - 1);
                  inBuf[offset++] = c;
                } 
                else if (c_state == C_STATE.HEADER_SIZE && offset >= dataSize) {
                  if (checksum == 0xFF - c) {  // compare calculated and transferred checksum
                    evaluateCommand(cmd, dataSize);  // we got a valid packet, evaluate it
                  }
                  else {
                    //
                      int a = 0;
                  }
                  c_state = C_STATE.IDLE;
                }
            }
            //this.Invoke(new EventHandler(redrawAll));
        }

        private void evaluateCommand(byte cmd, byte dataSize)
        {
            switch (cmd) {
                case CMD_SONARS:
                    sonars = ByteArrayToStructure<READ_SONARS>(inBuf);
                    this.Invoke(new EventHandler(processSonars));
                    break;
                //default: ;
            }
        }

        private void processString(object sender, EventArgs e)
        {
            chart1.Series[0].Points.RemoveAt(0);
            double tmp;
            try
            {
                tmp = Convert.ToDouble(RxChars);
            }
            catch (Exception ex)
            {
                tmp = 0d;
            }
            int nextY = (int)tmp;

            chart1.Series[0].Points.AddXY(nextX++, nextY);
            chart1.ChartAreas[0].AxisX.Minimum = nextX - MAX_POINTS;
            chart1.ChartAreas[0].AxisX.Maximum = nextX;
            chart1.ChartAreas[0].AxisY.Minimum = 0;
            chart1.ChartAreas[0].AxisY.Maximum = maxY;
            chart1.Invalidate();
        }

        private void processSonars(object sender, EventArgs e)
        {
            chart1.Series[0].Points.RemoveAt(0);
            double tmp;
            try
            {
                tmp = Convert.ToDouble(sonars.sonar1);
            }
            catch (Exception ex)
            {
                tmp = 0d;
            }
            int nextY = (int)tmp;
            listBox1.Items.Insert(0, nextY);
            listBox1.Invalidate();
            chart1.Series[0].Points.AddXY(nextX++, nextY);
            chart1.ChartAreas[0].AxisX.Minimum = nextX - MAX_POINTS;
            chart1.ChartAreas[0].AxisX.Maximum = nextX;
            chart1.ChartAreas[0].AxisY.Minimum = 0;
            chart1.ChartAreas[0].AxisY.Maximum = maxY;
            chart1.Invalidate();
        }

        private void redrawAll(object sender, EventArgs e)
        {
            listBox1.Items.Insert(0, RxChars);
            listBox1.Invalidate();
        }

        //Byte array to generic structure
        T ByteArrayToStructure<T>(byte[] bytes) where T : struct
        {
            GCHandle handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
            T stuff = (T)Marshal.PtrToStructure(handle.AddrOfPinnedObject(),
                typeof(T));
            handle.Free();
            return stuff;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        public void sendByteArray(byte[] ar, int count)
        {
            serialPort1.Write(ar, 0, count);
        }
    }
}
